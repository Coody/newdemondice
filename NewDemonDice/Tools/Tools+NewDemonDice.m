//
//  Tools+NewDemonDice.m
//  NewDemonDice
//
//  Created by CoodyChou on 2015/3/14.
//  Copyright (c) 2015年 CoodyChou. All rights reserved.
//

#import "Tools+NewDemonDice.h"

#import <UIKit/UIKit.h>

@implementation Tools_NewDemonDice

+(NSString *)getImagePathWithFileName:(NSString *)imageName withClassName:(NSString *)className
{
    NSString *imageString = ([NSString stringWithFormat:@"%@/ImageResources/%@/%@" , [NSBundle mainBundle].bundlePath , className , imageName]);
//    CGRect checkFrame = [[UIScreen mainScreen] bounds];
//    if ( checkFrame.size.height <= 480 ) {
//        imageString = [NSString stringWithFormat:@"%@-iPhone4",imageString];
//    }
//    else if( checkFrame.size.height <= 568 ){
//        imageString = [NSString stringWithFormat:@"%@-iPhone5",imageString];
//    }
//    else if( checkFrame.size.height <= 667 ){
//        imageString = [NSString stringWithFormat:@"%@-iPhone6",imageString];
//    }
//    else{
//        imageString = [NSString stringWithFormat:@"%@-iPhone6",imageString];
//    }
    return imageString;
}

@end
