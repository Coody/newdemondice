//
//  SKSpriteButton.m
//  NewDemonDice
//
//  Created by CoodyChou on 2015/4/4.
//  Copyright (c) 2015年 CoodyChou. All rights reserved.
//

#import "SKSpriteButton.h"

// for image
#import "Tools+NewDemonDice.h"

@interface SKSpriteButton()

@property (nonatomic, readonly) SEL actionTouchUpInside;
//@property (nonatomic, readonly) SEL actionTouchDown;
//@property (nonatomic, readonly) SEL actionTouchUp;
@property (nonatomic, readonly, weak) id targetTouchUpInside;
//@property (nonatomic, readonly, weak) id targetTouchDown;
//@property (nonatomic, readonly, weak) id targetTouchUp;
@property (nonatomic , strong) NSString *normalImage;
@property (nonatomic , strong) NSString *selectedImage;
@property (nonatomic , strong) NSString *mainClass;

@end

@implementation SKSpriteButton

#pragma -
#pragma mark Init with normal img & selected img
-(id)initWithNormalImage:(NSString *)tempNormalImage
       withSelectedImage:(NSString *)tempSelectedImage
               withClass:(NSString *)tempClass
{
    self = [super initWithImageNamed:GET_IMAGE_WITH_CLASS(tempNormalImage,tempClass)];
    if (self) {
        _normalImage = tempNormalImage;
        _selectedImage = tempSelectedImage;
        _mainClass = tempClass;
        _isSelected = NO;
        _isEnable = YES;
        [self setUserInteractionEnabled:YES];
        self.name = tempNormalImage;
    }
    return self;
}

-(void)setIsSelected:(BOOL)isSelected{
    _isSelected = isSelected;
    if ( isSelected == YES && _isEnable == YES ) {
        self.texture = [SKTexture textureWithImageNamed:GET_IMAGE_WITH_CLASS(_selectedImage,_mainClass)];
        // mover.texture = [SKTexture textureWithImageNamed:@"explode"];
    }
    else{
        self.texture = [SKTexture textureWithImageNamed:GET_IMAGE_WITH_CLASS(_normalImage,_mainClass)];
    }
}

#pragma -
#pragma mark Setting Target-Action pairs

- (void)setTouchUpInsideTarget:(id)target action:(SEL)action {
    _targetTouchUpInside = target;
    _actionTouchUpInside = action;
}



#pragma -
#pragma mark Touch Handling

/**
 * This method only occurs, if the touch was inside this node. Furthermore if
 * the Button is enabled, the texture should change to "selectedTexture".
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ( _isSelected == NO && _isEnable == YES) {
        [self setIsSelected:YES];
    }
    else{
        [self setIsSelected:NO];
    }
//    if ([self isEnabled]) {
//        if (_actionTouchDown){
//            [self.parent performSelectorOnMainThread:_actionTouchDown withObject:_targetTouchDown waitUntilDone:YES];
//            [self setIsSelected:YES];
//        }
//    }
}

/**
 * If the Button is enabled: This method looks, where the touch was moved to.
 * If the touch moves outside of the button, the isSelected property is restored
 * to NO and the texture changes to "normalTexture".
 */
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if ( _isSelected == YES && _isEnable == YES ) {
        UITouch *touch = [touches anyObject];
        CGPoint touchPoint = [touch locationInNode:self.parent];
        if ( CGRectContainsPoint(self.frame, touchPoint) ) {
            [self setIsSelected:YES];
        }
        else{
            [self setIsSelected:NO];
        }
    }
//    if ([self isEnabled]) {
//        UITouch *touch = [touches anyObject];
//        CGPoint touchPoint = [touch locationInNode:self.parent];
//        
//        if (CGRectContainsPoint(self.frame, touchPoint)) {
//            [self setIsSelected:YES];
//        } else {
//            [self setIsSelected:NO];
//        }
//    }
}

/**
 * If the Button is enabled AND the touch ended in the buttons frame, the
 * selector of the target is run.
 */
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInNode:self.parent];
    if ( [self isSelected] == YES ) {
        [self setIsSelected:NO];
        if ( CGRectContainsPoint(self.frame, touchPoint) && _actionTouchUpInside ) {
            [_targetTouchUpInside performSelectorOnMainThread:_actionTouchUpInside withObject:_targetTouchUpInside waitUntilDone:YES];
        }
    }
//    UITouch *touch = [touches anyObject];
//    CGPoint touchPoint = [touch locationInNode:self.parent];
//    
//    if ([self isEnabled] && CGRectContainsPoint(self.frame, touchPoint)) {
//        if (_actionTouchUpInside){
//            [self.parent performSelectorOnMainThread:_actionTouchUpInside withObject:_targetTouchUpInside waitUntilDone:YES];
//        }
//    }
//    [self setIsSelected:NO];
//    if (_actionTouchUp){
//        [self.parent performSelectorOnMainThread:_actionTouchUp withObject:_targetTouchUp waitUntilDone:YES];
//    }
}
@end
