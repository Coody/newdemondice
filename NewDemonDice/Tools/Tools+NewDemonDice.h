//
//  Tools+NewDemonDice.h
//  NewDemonDice
//
//  Created by CoodyChou on 2015/3/14.
//  Copyright (c) 2015年 CoodyChou. All rights reserved.
//

#import <Foundation/Foundation.h>

#define GET_IMAGE(img) [Tools_NewDemonDice getImagePathWithFileName:img withClassName:NSStringFromClass([self class])]
#define GET_IMAGE_WITH_CLASS(img,class) [Tools_NewDemonDice getImagePathWithFileName:img withClassName:class]

@interface Tools_NewDemonDice : NSObject

+(NSString *)getImagePathWithFileName:(NSString *)imageName withClassName:(NSString *)className;

@end
