//
//  SKSpriteButton.h
//  NewDemonDice
//
//  Created by CoodyChou on 2015/4/4.
//  Copyright (c) 2015年 CoodyChou. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SKSpriteButton : SKSpriteNode

@property (nonatomic) BOOL isSelected;
@property (nonatomic) BOOL isEnable;

-(id)initWithNormalImage:(NSString *)tempNormalImage
       withSelectedImage:(NSString *)tempSelectedImage
               withClass:(NSString *)tempClass;

/** Sets the target-action pair, that is called when the Button is tapped.
 "target" won't be retained.
 */
- (void)setTouchUpInsideTarget:(id)target action:(SEL)action;
//- (void)setTouchDownTarget:(id)target action:(SEL)action;
//- (void)setTouchUpTarget:(id)target action:(SEL)action;

@end
