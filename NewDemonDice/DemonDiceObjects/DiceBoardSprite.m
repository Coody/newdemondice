//
//  DiceBoardSprite.m
//  NewDemonDice
//
//  Created by CoodyChou on 2015/4/26.
//  Copyright (c) 2015年 CoodyChou. All rights reserved.
//

#import "DiceBoardSprite.h"

// for Button
#import "SKSpriteButton.h"

@interface DiceBoardSprite()

@property (nonatomic , strong) NSString *bgImageName;
@property (nonatomic , strong) SKSpriteNode *background;
@property (nonatomic , strong) SKSpriteButton *rubyButton;

@end

@implementation DiceBoardSprite

-(id)initWithbgImageName:(NSString *)tempBGImageName{
    self = [super init];
    if ( self ) {
        
        // 設定背景
        _bgImageName = tempBGImageName;
        _background = [[SKSpriteNode alloc] initWithImageNamed:_bgImageName];
        
        
    }
    return self;
}

@end
