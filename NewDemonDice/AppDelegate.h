//
//  AppDelegate.h
//  NewDemonDice
//
//  Created by CoodyChou on 2015/1/31.
//  Copyright (c) 2015年 CoodyChou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

