//
//  MainMenuScene.m
//  NewDemonDice
//
//  Created by CoodyChou on 2015/3/5.
//  Copyright (c) 2015年 CoodyChou. All rights reserved.
//

#import "MainMenuScene.h"

// for Sound
#import "SoundManager.h"

// for Tools
#import "Tools+NewDemonDice.h"
#import "SKSpriteButton.h"


@interface MainMenuScene()

@property (nonatomic , strong) SKSpriteButton *storyModeButton;
@property (nonatomic , strong) SKSpriteButton *quickPlayButton;
@property (nonatomic , strong) SKSpriteNode *storyAndQuickBtnBg;

@property (nonatomic , strong) SKSpriteButton *levelButtom;
@property (nonatomic , strong) SKSpriteButton *infinityButton;
@property (nonatomic , strong) SKSpriteNode *levelAndInfinityBtnBg;

@end

@implementation MainMenuScene


-(id)initWithSize:(CGSize)size{
    self = [super initWithSize:size];
    if ( self ) {
        
        self.scaleMode = SKSceneScaleModeFill;
        
//        [[SoundManager sharedManager] playMusic:@"startMainMenu.mp3" looping:YES];
//        
//        SKSpriteNode *background = [[SKSpriteNode alloc] initWithImageNamed:GET_IMAGE(@"MainMenuBackground")];
//        [background setSize:self.frame.size];
//        [background setPosition:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))];
//        background.name = @"MainMenuBackground";
//        [self addChild:background];
//        
//        // TODO: show DemonDice
//        [self initialDemonDiceLogo];
    }
    return self;
}

-(void)didMoveToView:(SKView *)view{
    
}


-(void)initialDemonDiceLogo{
    SKSpriteNode *demonDiceLogo = [[SKSpriteNode alloc] initWithImageNamed:[Tools_NewDemonDice getImagePathWithFileName:@"DemonDiceLogo" withClassName:NSStringFromClass([self class])]];
    [demonDiceLogo setPosition:CGPointMake(self.frame.size.width*0.5, self.frame.size.height*0.87)];
    [demonDiceLogo setZPosition:5.0f];
    [demonDiceLogo setAlpha:0.0f];
    [demonDiceLogo setScale:0.0f];
    SKAction *popBigAction = [SKAction scaleTo:1.1f duration:0.3f];
    SKAction *popSmallAction = [SKAction scaleTo:1.0f duration:0.15f];
    SKAction *initialButtonSelectAction = [SKAction performSelector:@selector(initialStoryAndQuickButton)
                                                           onTarget:self];
    [demonDiceLogo runAction:[SKAction sequence:@[popBigAction , popSmallAction , initialButtonSelectAction]]];
    [demonDiceLogo runAction:[SKAction fadeInWithDuration:0.3f]];
    [self addChild:demonDiceLogo];
}

//-(void)initialStoryAndQuickButton{
//    //
//    if ( _storyAndQuickBtnBg == nil ) {
//        _storyAndQuickBtnBg = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor]
//                                                             size:CGSizeMake(self.frame.size.width,
//                                                                             self.frame.size.height*0.1)];
//        [_storyAndQuickBtnBg setZPosition:1.0f];
//        [_storyAndQuickBtnBg setUserInteractionEnabled:NO];
//        [self addChild:_storyAndQuickBtnBg];
//    }
//    [_storyAndQuickBtnBg setPosition:CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height*0.78)];
//    [_storyAndQuickBtnBg setAlpha:0.0f];
//    
//    // story Button
//    if ( _storyModeButton == nil ) {
//        _storyModeButton = [[SKSpriteButton alloc] initWithNormalImage:@"storyModeButton"
//                                                     withSelectedImage:@"storyModeButtonSelected"
//                                                             withClass:@"MainMenuScene"];
//        [_storyModeButton setTouchUpInsideTarget:self action:@selector(changeSceneToStoryMode)];
//        _storyModeButton.name = @"storyModeButton";
//        [_storyAndQuickBtnBg addChild:_storyModeButton];
//    }
//    
//    [_storyModeButton setPosition:CGPointMake(_storyAndQuickBtnBg.frame.size.width*0.2*-1,0)];
//    
//    
//    // quick Play Button
//    if ( _quickPlayButton == nil ) {
//        _quickPlayButton = [[SKSpriteButton alloc] initWithNormalImage:@"quickPlayModeButton"
//                                                     withSelectedImage:@"quickPlayModeButtonSelected"
//                                                             withClass:@"MainMenuScene"];
//        [_quickPlayButton setTouchUpInsideTarget:self action:@selector(initialMenuWithLevelModeAndInfinityMode)];
//        [_storyAndQuickBtnBg addChild:_quickPlayButton];
//    }
//    [_quickPlayButton setPosition:CGPointMake(_storyAndQuickBtnBg.frame.size.width*0.2,0)];
//    
//    
//    
//    
//    // Buttons Action
//    SKAction *moveDownAction = [SKAction moveTo:CGPointMake(self.frame.size.width*0.5, self.frame.size.height*0.73)
//                                       duration:0.3f];
//    SKAction *fadeInAction = [SKAction fadeInWithDuration:0.15f];
//    [_storyAndQuickBtnBg runAction:moveDownAction];
//    [_storyAndQuickBtnBg runAction:fadeInAction];
//    
//    _storyModeButton.isEnable = YES;
//    _quickPlayButton.isEnable = YES;
//}
//
//
//
//-(void)initialMenuWithLevelModeAndInfinityMode{
//    _storyModeButton.isEnable = NO;
//    _quickPlayButton.isEnable = NO;
//    
//    // 收起 story Button & quick play button
//    SKAction *fadeOutAction = [SKAction fadeOutWithDuration:0.15f];
//    SKAction *moveUpAction = [SKAction moveTo:CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height*0.78)
//                                      duration:0.3f];
//    [_storyAndQuickBtnBg runAction:fadeOutAction];
//    __weak __typeof(self) weakSelf = self;
//    [_storyAndQuickBtnBg runAction:moveUpAction completion:^{
//        __strong __typeof(weakSelf) strongSelf = weakSelf;
//        
//        // level and infinity button background
//        if ( strongSelf->_levelAndInfinityBtnBg == nil ) {
//            strongSelf->_levelAndInfinityBtnBg = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor]
//                                                                    size:CGSizeMake(strongSelf.frame.size.width,
//                                                                                    strongSelf.frame.size.height*0.1)];
//            [strongSelf->_levelAndInfinityBtnBg setZPosition:1.0f];
//            [strongSelf->_levelAndInfinityBtnBg setUserInteractionEnabled:NO];
//            [strongSelf addChild:strongSelf->_levelAndInfinityBtnBg];
//        }
//        [strongSelf->_levelAndInfinityBtnBg setPosition:CGPointMake(CGRectGetMidX(strongSelf.frame),
//                                                        strongSelf.frame.size.height*0.78)];
//        [strongSelf->_levelAndInfinityBtnBg setAlpha:0.0f];
//        
//        // Level Button
//        if ( strongSelf->_levelButtom == nil ) {
//            strongSelf->_levelButtom = [[SKSpriteButton alloc] initWithNormalImage:@"quickPlayModeButton"
//                                                           withSelectedImage:@"quickPlayModeButtonSelected"
//                                                                   withClass:@"MainMenuScene"];
//            [strongSelf->_levelButtom setTouchUpInsideTarget:strongSelf
//                                                      action:@selector(changeSceneToLevelMode)];
//            [strongSelf addChild:strongSelf->_levelButtom];
//        }
//        [strongSelf->_levelButtom setPosition:CGPointMake(strongSelf.frame.size.width*0.29, strongSelf.frame.size.height*0.78)];
//        [strongSelf->_levelButtom setZPosition:1.0f];
//        [strongSelf->_levelButtom setAlpha:0.0f];
//        
//        
//        // infinity button
//        if ( strongSelf->_infinityButton == nil ) {
//            strongSelf->_infinityButton = [[SKSpriteButton alloc] initWithNormalImage:@"storyModeButton"
//                                                        withSelectedImage:@"storyModeButtonSelected"
//                                                                withClass:@"MainMenuScene"];
//            [strongSelf->_infinityButton setPosition:CGPointMake(strongSelf.frame.size.width*0.71, strongSelf.frame.size.height*0.78)];
//            [strongSelf->_infinityButton setTouchUpInsideTarget:strongSelf
//                                                         action:@selector(changeSceneToInfinityMode)];
//            [strongSelf addChild:strongSelf->_infinityButton];
//        }
//        [strongSelf->_infinityButton setZPosition:1.0f];
//        [strongSelf->_infinityButton setAlpha:0.0f];
//        
//        // actions
//        SKAction *fadeInAction = [SKAction fadeInWithDuration:0.15f];
//        SKAction *moveDownAction1 = [SKAction moveTo:CGPointMake(self.frame.size.width*0.29, self.frame.size.height*0.73)
//                                            duration:0.3f];
//        SKAction *moveDownAction2 = [SKAction moveTo:CGPointMake(self.frame.size.width*0.71, self.frame.size.height*0.73)
//                                            duration:0.3f];
//        [strongSelf->_levelButtom runAction:fadeInAction];
//        [strongSelf->_levelButtom runAction:moveDownAction1];
//        [strongSelf->_infinityButton runAction:fadeInAction];
//        [strongSelf->_infinityButton runAction:moveDownAction2];
//    }];
//}


//#pragma mark - 
//#pragma mark Change Scene
//-(void)changeSceneToStoryMode{
//    NSLog(@"changeSceneToStoryMode !!!!");
//}
//
//-(void)changeSceneToLevelMode{
//    NSLog(@"changeSceneToLevelMode");
//}
//
//-(void)changeSceneToInfinityMode{
//    NSLog(@"changeSceneToInfinityMode");
//}

#pragma mark -
#pragma mark Touches Event
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    //if fire button touched, bring the rain
    if ([node.name isEqualToString:@""]) {
        [node removeFromParent];
        node = nil;
    }
}


@end
