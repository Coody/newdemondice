//
//  GameViewController.m
//  NewDemonDice
//
//  Created by CoodyChou on 2015/1/31.
//  Copyright (c) 2015年 CoodyChou. All rights reserved.
//

#import "GameViewController.h"
#import "LogoScene.h"

@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    return scene;
}

@end

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    skView.ignoresSiblingOrder = YES;
        
    SKTransition *transitionToMainMenuScene = [SKTransition fadeWithDuration:1.0f];
    SKScene *logoScene = [[LogoScene alloc] initWithSize:self.view.frame.size];
    logoScene.scaleMode = SKSceneScaleModeResizeFill;
    [skView presentScene:logoScene transition:transitionToMainMenuScene];
    
//    // Create and configure the scene.
//    LogoScene *logoScene = [LogoScene unarchiveFromFile:@"LogoScene"];
//    logoScene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
//    [skView presentScene:logoScene];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
