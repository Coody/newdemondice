//
//  LogoScene.m
//  NewDemonDice
//
//  Created by CoodyChou on 2015/3/5.
//  Copyright (c) 2015年 CoodyChou. All rights reserved.
//

#import "LogoScene.h"
#import "MainMenuScene.h"

// Sounds
#import "SoundManager.h"

// Tools
#import "Tools+NewDemonDice.h"

@implementation LogoScene

-(instancetype)initWithSize:(CGSize)size{
    self = [super initWithSize:size];
    if (self) {
        self.scaleMode = SKSceneScaleModeAspectFit;
        
        [self setBackgroundColor:[UIColor whiteColor]];
        SKSpriteNode *logoSpriteNode = [[SKSpriteNode alloc] initWithImageNamed:[Tools_NewDemonDice getImagePathWithFileName:@"NORMI_png" withClassName:NSStringFromClass([self class])]];
        
        [logoSpriteNode setPosition:CGPointMake(self.frame.size.width*0.5,
                                                self.frame.size.height*0.5 )];
        [logoSpriteNode setAlpha:0.0f];
        [logoSpriteNode setScale:0.9f];
        SKAction *stopAction = [SKAction waitForDuration:1.3f];
        SKAction *stopAction2 = [SKAction waitForDuration:3.5f];
        SKAction *fadeInAction = [SKAction fadeAlphaTo:1.0f duration:0.2f];
        SKAction *changeScenenAction = [SKAction performSelector:@selector(changeSceneToMainMenuScene) onTarget:self];
        SKAction *sequenceAction = [SKAction sequence:@[stopAction , fadeInAction , stopAction2 , changeScenenAction]];
        [logoSpriteNode runAction:sequenceAction];
        
        SKAction *soundSelectAction = [SKAction runBlock:^{
            [[SoundManager sharedManager] playMusic:@"logo.mp3" looping:NO];
        }];
        SKAction *soundSequenceAction = [SKAction sequence:@[stopAction , soundSelectAction]];
        //    SKAction *soundAction = [SKAction playSoundFileNamed:@"logo.mp3" waitForCompletion:NO];
        //    SKAction *soundSequenceAction = [SKAction sequence:@[stopAction , soundAction]];
        [logoSpriteNode runAction:soundSequenceAction];
        
        [self addChild:logoSpriteNode];
        
        // test
        CGFloat scale = [UIScreen mainScreen].scale;
        NSLog(@" Scale = %f" , scale );
    }
    return self;
}

-(void)didMoveToView:(SKView *)view{
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self changeSceneToMainMenuScene];
}

-(void)changeSceneToMainMenuScene{
    SKTransition *transitionToMainMenuScene = [SKTransition fadeWithDuration:1.0f];
    SKScene *mainMenuScene = [[MainMenuScene alloc] initWithSize:self.frame.size];
    mainMenuScene.scaleMode = SKSceneScaleModeResizeFill;
    [self.view presentScene:mainMenuScene transition:transitionToMainMenuScene];
}

@end
